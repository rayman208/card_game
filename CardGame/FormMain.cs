﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGame
{
    public partial class FormMain : Form
    {
        const char GUESSED_SYMBOL = 'x';
        const char EMPTY_SYMBOL = '.';

        const int FIELD_N = 4;
        const int FIELD_M = 5;
        const int SIZE_CELL = 80;

        Random rnd = new Random();

        char[,] field = new char[FIELD_N, FIELD_M];
        int gameStep = 0;
        int i1 = -1, j1 = -1, i2 = -1, j2 = -1;


        Bitmap bitmapField;
        Graphics graph;


        private void DrawFieldToBitmap()
        {
            graph.Clear(Color.White);

            for (int i = 0; i < FIELD_N; i++)
            {
                for (int j = 0; j < FIELD_M; j++)
                {
                    if (field[i, j] == GUESSED_SYMBOL)
                    {
                        graph.FillRectangle(Brushes.Red, j * SIZE_CELL, i * SIZE_CELL, SIZE_CELL, SIZE_CELL);
                    }
                    else
                    {
                        graph.DrawRectangle(Pens.Black, j * SIZE_CELL, i * SIZE_CELL, SIZE_CELL, SIZE_CELL);
                    }

                    if (i == i1 && j == j1 || i == i2 && j == j2)
                    {
                        graph.DrawString(
                            field[i, j].ToString(),
                            new Font("Arial",25), Brushes.Black,
                            j * SIZE_CELL + SIZE_CELL / 4,
                            i * SIZE_CELL + SIZE_CELL / 4);
                    }

                }
            }
        }

        private void pictureBoxField_MouseDown(object sender, MouseEventArgs e)
        {
            int iClick = e.Y / SIZE_CELL;
            int jClick = e.X / SIZE_CELL;
            
            if(field[iClick,jClick]==GUESSED_SYMBOL ||
               iClick==i1 && jClick==j1)
            {
                MessageBox.Show("Клик в эту ячейку невозможен");
                return;
            }

            gameStep++;
            if(gameStep==1)
            {
                i1 = iClick;
                j1 = jClick;
                DrawFieldToBitmap();
                pictureBoxField.Image = bitmapField;
            }
            if(gameStep==2)
            {
                i2 = iClick;
                j2 = jClick;
                DrawFieldToBitmap();
                pictureBoxField.Image = bitmapField;
                pictureBoxField.Refresh();

                System.Threading.Thread.Sleep(500);

                if(field[i1,j1]==field[i2,j2])
                {
                    field[i1, j1] = GUESSED_SYMBOL;
                    field[i2, j2] = GUESSED_SYMBOL;
                }

                gameStep = 0;
                i1 = j1 = i2 = j2 = -1;
                DrawFieldToBitmap();
                pictureBoxField.Image = bitmapField;
                pictureBoxField.Refresh();

                System.Threading.Thread.Sleep(500);

                int countCell = 0;
                for (int i = 0; i < FIELD_N; i++)
                {
                    for (int j = 0; j < FIELD_M; j++)
                    {
                        if(field[i,j]==GUESSED_SYMBOL)
                        {
                            countCell++;
                        }
                    }
                }
                if(countCell==FIELD_N*FIELD_M)
                {
                    MessageBox.Show("Поздравляю, Вы поебдили!");
                }

            }

        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            bitmapField = new Bitmap(pictureBoxField.Width, pictureBoxField.Height);
            graph = Graphics.FromImage(bitmapField);
        }

        private void buttonRestartGame_Click(object sender, EventArgs e)
        {
            gameStep = 0;
            i1 = j1 = i2 = j2 = -1;
            graph.Clear(Color.White);

            for (int i = 0; i < FIELD_N; i++)
            {
                for (int j = 0; j < FIELD_M; j++)
                {
                    field[i, j] = EMPTY_SYMBOL;
                }
            }

            int count = FIELD_N * FIELD_M / 2;

            for (int k = 0; k < count; k++)
            {
                char tempSymb = (char)rnd.Next('А', 'Я');
                for (int kk = 0; kk < 2; kk++)
                {
                    int i, j;
                    do
                    {
                        i = rnd.Next(0, FIELD_N);
                        j = rnd.Next(0, FIELD_M);
                    }
                    while (field[i, j] != EMPTY_SYMBOL);
                    field[i, j] = tempSymb;
                }
            }

            DrawFieldToBitmap();
            pictureBoxField.Image = bitmapField;
        }
    }
}
